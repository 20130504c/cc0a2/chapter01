/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.valegrei.p22050702;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Victor Alegre <victoralegre@uni.pe>
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Files");
        //https://www.asciitable.com/
        String name = "ByteStream.txt";
        File file = new File(name);
        System.out.println("Location of file: "+file.getAbsolutePath());
        
        System.out.println("Writting a file");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(86);
        fileOutputStream.write(105);
        fileOutputStream.write(99);
        fileOutputStream.write(116);
        fileOutputStream.write(111);
        fileOutputStream.write(114);
        fileOutputStream.close();
        
        System.out.println("Read the file");
        FileInputStream fileInputStream = new FileInputStream(file);
        int decimal;
        while((decimal = fileInputStream.read()) != -1 ){
            System.out.print((char) decimal);
        }
        System.out.println("");
        
        name = "CharacterStream.txt";
        file = new File(name);
        System.out.println("Location of file: "+file.getAbsolutePath());
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(86);
        fileWriter.write(105);
        fileWriter.write(99);
        fileWriter.write(116);
        fileWriter.write(111);
        fileWriter.write(114);
        fileWriter.close();
        
        System.out.println("Read the file");
        FileReader fileReader = new FileReader(file);
        while((decimal = fileReader.read()) != -1){
            System.out.print((char) decimal);
        }
        System.out.println("");
        
        System.out.println("Read the file by line");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while((line = bufferedReader.readLine())!=null){
            System.out.println(line);
        }
        
        System.out.println("List of files");
        String paths[];
        file = new File("./");
        System.out.println("Directory: "+file.getAbsolutePath());
        paths = file.list();
        for (String path : paths) {
            System.out.println(path);
        }
        
        System.out.println("Creating directories");
        String directory = "/Files/Binaries/Selected";
        String fullPath = file.getAbsolutePath()+directory;
        file = new File(fullPath);
        if(file.mkdirs()){
            System.out.println("Directories has been created");
        }else{
            System.out.println("Directories has already been created");
        }
    }
}
