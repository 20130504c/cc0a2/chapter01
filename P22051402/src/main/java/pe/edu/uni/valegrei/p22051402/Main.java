/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.valegrei.p22051402;

/**
 *
 * @author Victor Alegre <victoralegre@uni.pe>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Exception 2 ...");
        try {
            myExceptionFuncion();
            function();
            System.out.println("End of try ...");
        } catch (ArithmeticException e) {
            System.out.println("Arithmetic Exception: "+e);
        } catch (MyException e) {
            System.out.println("My Exception: "+e);
        } catch (Exception e) {
            System.out.println("Exception: "+e);
        }
        System.out.println("End ...");
    }
    
    public static void function() throws ArithmeticException{
        int i = 1/0;
    }
    
    public static void myExceptionFuncion() throws MyException{
        throw new MyException(" My error !!!");
    }
}
