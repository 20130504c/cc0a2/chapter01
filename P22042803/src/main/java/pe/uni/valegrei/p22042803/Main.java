/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.valegrei.p22042803;

/**
 *
 * @author Victor Alegre <victoralegre@uni.pe>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("conditional statements");
        /*int i = 30;
        if (30 < i){
            System.out.println("The number is greater to 30");
        }
        if (i % 2 == 1){
            System.out.println("The number is odd");
        }
        if (i % 5 == 0){
            System.out.println("The number is multple of 5");
//        }*/
        /*
        int i = 30;
        if (30 < i){
            System.out.println("The number is greater to 30");
        } else
        if (i % 2 == 1){
            System.out.println("The number is odd");
        } else
        if (i % 5 == 0){
            System.out.println("The number is multple of 5");
        } else{
            System.out.println("The number is other");
        }
        System.out.println("Here the program continue");
//        */
//        /*
        int i = 28;
        if (30 < i){
            System.out.println("The number is greater to 30, and the program ends");
            return;
        }
        if (i % 2 == 1){
            System.out.println("The number is odd, and the program ends");
            return;
        }
        if (i % 5 == 0){
            System.out.println("The number is multple of 5, and the program ends");
            return;
        }
        System.out.println("Here the program continue");
//        */
    }
}
