/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.valegrei.p22050701;

/**
 *
 * @author Victor Alegre <victoralegre@uni.pe>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Functions");
        function();
        function("Victor");
        function("Victor",27);
        String[] result = {"Hello ","Victor",","," you"," are"," 27"," years"," old"," !!!"};
        function(result);
        //int sum = function(1, 2, 3, 4, 5, 6, 7, 8, 9);
        //System.out.println("Sum: "+String.valueOf(sum));
        int a = 2;
        int b = 3;
        function(a,b);
        System.out.println("a: "+String.valueOf(a)+", b: "+String.valueOf(b));
        Integer a1=2,b1=3;
        function(a1,b1);
        System.out.println("a: "+String.valueOf(a1)+", b: "+String.valueOf(b1));
        
        StringBuilder stringNumber = new StringBuilder();
        stringNumber.setLength(0);
        stringNumber.append("0");
        System.out.println("StringBuilder: "+stringNumber.toString());
        function(stringNumber);
        System.out.println("StringBuilder: "+stringNumber.toString());
        
        stringNumber.setLength(0);
        if(authentication("2", stringNumber)){
            System.out.println("Authentication is ok !!!");
        }else{
            System.out.println("Authentication failed, error: "+ stringNumber);
        }
        
        Clase1 cl1 = new Clase1();
        System.out.println(cl1.toString());
        prueba(cl1);
        System.out.println(cl1.toString());
    }
    
    //Sobrecarga afecta a nombre de funcion y tipos de argumentos
    public static void function(){
        System.out.println("Hello world !!!");
    }
    
    public static void function(String name){
        System.out.println("Hello "+name+" !!!");
    }
    
    public static void function(String name,int age){
        System.out.println("Hello "+name+", you are " +String.valueOf(age)+ " years old !!!");
    }
    
    public static void function(String[] args){
        for (String arg : args) {
            System.out.print(arg);
        }
        System.out.println("");
    }
    
    // suma (int a, int b)
    /**
     * Add many numbers
     * @param numbers every numbers int the arguments
     * @return sum of numbers
     */

    public static int function(int... numbers){
        int sum = 0;
        for(int number : numbers){
            sum += number;
            number += 10;
        }
        return sum;
    }
  
    public static void function(StringBuilder reference){
        reference.setLength(0);
        reference.append("1");
    }
    
    /**
     * 
     * @param credential
     * @param error
     * @return 
     */
    public static boolean authentication(String credential, StringBuilder error){
        boolean out = false;
        error.setLength(0);
        if(credential.equals("1")){
            out = true;
            error.append("0");  // authentication is ok
        }else{
            error.append("-1"); // otherwise
        }
        return out;
    }
    
    public static void prueba(Clase1 a){
        a.a = 12;
    }
    
    public static class Clase1{
        int a=0;
        int b=0;

        @Override
        public String toString() {
            return "a = "+a+", b ="+b; //To change body of generated methods, choose Tools | Templates.
        }
        
    }
}
