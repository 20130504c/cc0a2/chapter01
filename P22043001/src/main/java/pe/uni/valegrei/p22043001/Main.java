/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.valegrei.p22043001;

/**
 *
 * @author Victor Alegre <victoralegre@uni.pe>
 */
public class Main {

    public static void main(String[] args) {
        int f = 5, c = 3;
        
        System.out.println("Iteration");
        int i = 0;
        while(i<0){
            System.out.println("This while message is not printed");
        }
        
        do{
            System.out.println("This do while message is printed");
        }while(i<0);
        
        i = 1;
        do{
            int j = 1;
            do{
                System.out.print("(" + i + ", " + j + ")\t");
                j++;
            }while(j <= c);
            System.out.println("");
            i++;
        }while(i <= f);
        
        // print prime numbers between 10 and 90
        // n -> n/1 = n
        
        for(int p = 10; p <= 90; p++){
            if(p==53) continue;
            boolean t = true;
            for(int j = 2; j < p; j++ ){
                if(p%j==0){
                    t = false;
                    break;
                }
            }
            if(t){
                System.out.println("The number is prime: "+p);
            }
        }
        
    }
    
}
